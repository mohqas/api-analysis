from configurations.naming.submissions import collectionIndex, submittedBinaryCategory, binaryFamilyVersion, isAnalyzed, sandbox
from submitter.persisters.mongodb.BasePersister import BasePersister


class SubmissionsPersister(BasePersister):
    def __init__(self, databaseConfiguration):
        super(SubmissionsPersister, self).__init__(databaseConfiguration.analysisDatabase)
        self.submittedBinaries = self.db[databaseConfiguration.binariesAnalysisIDCollection]

    def persistSubmittedBinary(self, analysisID, category, version="null", sandboxName="All"):
        """
        The collection uses the analysis ID as the index. Therefore, redoing analysis is safe (overrides whatever was on that ID)
        :param analysisID: The ID of the analysis in Cuckoo storage
        :param category: The classification of the binary (benign, unknown, malware, or a malware family)
        :param version: Optional, only used if the category is set as a malware family and the version is available.
        :param sandboxName: The sandbox on which the analysis is to be carried.
        :return: None
        """
        self.submittedBinaries.update({collectionIndex: analysisID},
                                      {collectionIndex: analysisID, submittedBinaryCategory: category, binaryFamilyVersion: version,
                                       sandbox: sandboxName, isAnalyzed: False}, upsert=True)

    def getNotAnalyzedBinaries(self, sandboxName=None):
        """
        Retrieves a list of binaries which Cuckoo report has not been processed yet.
        :return: A list of binary documents (analysis index, family, version). The analysis index is used to find the report in Cuckoo
        storage.
        """
        inquiry = {isAnalyzed: False}
        if sandboxName:
            inquiry[sandbox] = sandboxName
        return list(self.submittedBinaries.find(inquiry))

    def setBinaryAnalyzed(self, analysisId):
        """
        Sets the provided analysisId as already analysis. This is done after a full successful analysis of the analysisId JSON report
        which resulted from Cuckoo run.
        :param analysisId: The ID of the analysis which JSON report was successfully inspected by ApiInspect
        :return: None
        """
        self.submittedBinaries.find_one_and_update({collectionIndex: analysisId}, {"$set": {isAnalyzed: True}})
