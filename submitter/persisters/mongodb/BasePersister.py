import pymongo

from configurations.Logger import getLogger



# TODO: Integrate into the SubmissionsPersister. No longer needed in this design
class BasePersister(object):
    def __init__(self, database):
        client = pymongo.MongoClient(socketKeepAlive=True)
        self.db = client[database]
        self.logger = getLogger()





