from submitter.persisters.mongodb.SubmissionsPersister import SubmissionsPersister


class SandboxInterface(object):
    def __init__(self, dbConfiguration):
        self.name = None
        self.persister = SubmissionsPersister(dbConfiguration)

    def submitBinary(self, binaryPath, family, version):
        raise NotImplementedError

    def _persistBinarySubmission(self, submissionId, family, version):
        self.persister.persistSubmittedBinary(submissionId, family, version, sandboxName=self.name)
