import subprocess
from configurations.sandboxes.CuckooConfiguration import CuckooConfiguration
from submitter.interfaces.SandboxInterface import SandboxInterface


class CuckooInterface(SandboxInterface):
    def __init__(self, dbConfiguration):
        super(CuckooInterface, self).__init__(dbConfiguration)
        self.config = CuckooConfiguration()
        self.name = self.config.name

    def submitBinary(self, binaryPath, family, version):
        # TODO: Switch to use Cuckoo API (lib.cuckoo.core.db.add_path)
        output = subprocess.check_output(["python", self.config.cuckooSubmitBinaryPath, binaryPath,
                                          "--timeout", str(self.config.analysisTimeout)])
        submissionId = output.strip().split()[-1]

        self._persistBinarySubmission(submissionId, family, version)
