#!/usr/bin/python
import argparse
from configurations.database import DatabaseConfiguration
from submitter.interfaces.CuckooInterface import CuckooInterface


def parseArguments():
    parser = argparse.ArgumentParser(description='Submit an analysis to the Cuckoo Sandbox.')
    parser.add_argument('binary', type=str,
                        help='the binary to analyse')
    parser.add_argument('family', nargs="?", default='unknown',
                        help='the classification for the executable (unknown -default-, or family)')
    parser.add_argument("version", nargs="?", default="unknown",
                        help="The version of the executable family if the family is used as a category")
    return parser.parse_args()


if __name__ == "__main__":
    args = parseArguments()
    interface = CuckooInterface(DatabaseConfiguration())
    interface.submitBinary(args.binary, args.family, args.version)
