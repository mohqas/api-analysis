import unittest
import os

from acquisition.parsers.cuckoo.JsonReportParser import ProcessParser, JsonReportParser


TESTFILE = os.path.join(os.path.dirname(__file__), "../../testData/report.json")


class TestJsonReportParser(unittest.TestCase):
    def test_reportParser_readsCorrectNumberOfProcesses(self):
        processes = [a for a in JsonReportParser(TESTFILE).processIter()]
        process = processes[0]

        self.assertEqual(1, len(processes))
        self.assertEqual("fe3d96db70d5362a804a52adf7b4aece", process.binaryMd5)
        self.assertEqual("test_process.exe", process.binaryName)

    def test_processParser_readsApiCallsOfAProcessCorrectly(self):
        reader = JsonReportParser(TESTFILE)
        process = reader.processIter().next()
        self.assertEqual(8, len([a for a in ProcessParser.apiIter(process)]))
