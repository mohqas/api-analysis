import json
from collections import namedtuple
from configurations.naming.cuckooReport import parentId, processId, processName, firstSeen, callsList, targetInfo, fileInfo, nameField, \
    md5Field, behaviorInfo, processesDetails, apiArguments, callTimestamp, callCategory, callStatus, threadId, callReturnValue, isRepeated, \
    apiName

Process = namedtuple("Process", ["parentId", "processId", "processName", "firstSeen", "calls", "binaryName", "binaryMd5"])
ApiCall = namedtuple("ApiCall", ["category", "status", "returnValue", "timestamp", "threadId", "repeated", "api", "arguments"])
ApiArgument = namedtuple("apiArgument", ["name", "value"])


class JsonReportParser(object):
    def __init__(self, reportFN):
        self.report = reportFN

    # Assuming we have only files for now, and no links
    @staticmethod
    def _getProcessName(reportJson):
        return reportJson.get(targetInfo).get(fileInfo).get(nameField)

    @staticmethod
    def _getProcessMD5(reportJson):
        return reportJson.get(targetInfo).get(fileInfo).get(md5Field)

    def processIter(self):
        reportJson = json.load(open(self.report))
        behavior = reportJson.get(behaviorInfo, {})
        processes = behavior.get(processesDetails, [])
        for p in processes:
            # TODO: Memory issues, can we yield an Iterator?
            yield Process(
                parentId=p[parentId],
                processId=p[processId],
                processName=p[processName],
                firstSeen=p[firstSeen],
                calls=p[callsList],
                binaryName=self._getProcessName(reportJson),
                binaryMd5=self._getProcessMD5(reportJson),
            )


class ProcessParser(object):
    @staticmethod
    def apiIter(processInfo):
        calls = processInfo.calls
        for c in calls:
            yield ApiCall(
                category=c.get(callCategory), status=c.get(callStatus), returnValue=c.get(callReturnValue),
                timestamp=c.get(callTimestamp), threadId=c.get(threadId), repeated=c.get(isRepeated), api=c.get(apiName),
                arguments=c.get(apiArguments, {}).items()
            )


