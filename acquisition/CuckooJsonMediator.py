from acquisition.formatters.ApiArgsSequence import ApiArgsSequence
from acquisition.formatters.ApiSequence import ApiSequence
from acquisition.parsers.cuckoo.JsonReportParser import JsonReportParser, ProcessParser
from acquisition.persisters.mongodb.ArgsPersister import ArgsPersister
from acquisition.persisters.mongodb.BinariesCallSequencesPersister import BinariesCallSequencesPersister
from acquisition.persisters.mongodb.CallsPersister import CallsPersister
from acquisition.persisters.plotters.TimeSeriesPlotter import PlotApiSequence
from configurations.Logger import getLogger


# TODO: A proper acceptance test to ensure that the order of read Apis is persisted in the Json parser across multiples runs!


class CuckooJsonMediator(object):
    def __init__(self, databaseConfiguration, plotConfiguration):
        self.logger = getLogger()

        self.apiPersister = CallsPersister(databaseConfiguration)
        self.argsPersister = ArgsPersister(databaseConfiguration)

        self.sequenceCreator = ApiSequence(databaseConfiguration)
        self.sequenceWithArgsCreator = ApiArgsSequence(databaseConfiguration)

        self.binariesSequencesPersister = BinariesCallSequencesPersister(databaseConfiguration)

        self.plotsLoc = "%s%s" % (plotConfiguration.resultsLocation, databaseConfiguration.analysisDatabase)

    @staticmethod
    def _getProcessesOfReport(reportLocation):
        return JsonReportParser(reportLocation).processIter()

    @staticmethod
    def _getApiCallsOfProcess(processInfo):
        return ProcessParser.apiIter(processInfo)

    def _persistCall(self, apiDetails):
        self.apiPersister.persistCall(apiDetails)

    def _persistArgs(self, apiDetails):
        for arg in apiDetails.arguments:
            self.argsPersister.persistArg(arg)

    def _createApiSequenceForCalls(self, apiCalls):
        return self.sequenceCreator.createSequence(apiCalls)


    def _createPlot(self, processInfo, sequence, family, version):
        PlotApiSequence(self.plotsLoc).plotProcess(self.binariesSequencesPersister._createProcessDocument(family, processInfo, sequence, version))

    def _updateIndexes(self, processApiCallsIter):
        # Update API and Args Index
        for apiCall in processApiCallsIter:
            self._persistCall(apiCall)
            self._persistArgs(apiCall)

    def _persistCallsAndSequenceOfProcess(self, binaryFamily, familyVersion, process):
        # Create API Inspect sequence and persist it
        processApiCallsIter = self._getApiCallsOfProcess(process)
        apiCallsSequence = self._createApiSequenceForCalls(processApiCallsIter)
        self.binariesSequencesPersister.persistProcessSequence(process, apiCallsSequence, binaryFamily, familyVersion)
        self.logger.debug("Read process APIs: %s:", apiCallsSequence)

        # Plot
        # self._createPlot(process, apiCallsSequence, binaryFamily, familyVersion)

        # API - Args sequences
        processApiCallsIter = self._getApiCallsOfProcess(process)
        apiWithArgsSequence = self.sequenceWithArgsCreator.createSequence(processApiCallsIter)
        self.binariesSequencesPersister.persistProcessWithArgsSequence(process, apiWithArgsSequence, binaryFamily, familyVersion)

        # Create further representations
        # behaviorGraph = self._createBehaviorGraphForCalls(processApiCalls)
        # self._persistBehaviorGraph(behaviorGraph)
        # self.logger.debug("Done Graph")

        # Abstract?

    def processSequencesOfReport(self, reportPath, binaryFamily, familyVersion):
        try:
            processes = [p for p in self._getProcessesOfReport(reportPath)]
        except ValueError:
            self.logger.warn("Broken JSON file found. Cuckoo run was interrupted maybe?")
            return False

        if len(processes) == 0:
            self.logger.info("No processes found in given report!")
            return False

        self.logger.info("Processing report: %s", reportPath)

        for process in processes:
            self.logger.info("Processing process: %s of binary: %s", process.processName, process.binaryName)
            processApiCallsIter = self._getApiCallsOfProcess(process)
            self._updateIndexes(processApiCallsIter)
            self._persistCallsAndSequenceOfProcess(binaryFamily, familyVersion, process)

        return True

