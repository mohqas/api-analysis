from acquisition.persisters.mongodb.CallsPersister import CallsPersister


class ApiSequence(object):
    def __init__(self, databaseConfiguration):
        self.callsPersister = CallsPersister(databaseConfiguration)

    def createSequence(self, apiCalls):
        """
        :param apiCalls: A list of APIs: The named tuple in JsonReporterReader.
        :return: A time series (ordered list of APIs index in the DB).
        """
        return [self.callsPersister.getCallIndex(call) for call in apiCalls]
