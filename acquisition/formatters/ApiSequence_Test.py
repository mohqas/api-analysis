import unittest
import os
from configurations.database import DatabaseConfiguration
from acquisition.formatters.ApiSequence import ApiSequence
from acquisition.parsers.cuckoo.JsonReportParser import JsonReportParser, ProcessParser

TESTFILE = os.path.join(os.path.dirname(__file__), "../testData/report.json")


class TestApiSequence(unittest.TestCase):
    def setUp(self):
        process = JsonReportParser(TESTFILE).processIter().next()
        self.apiCalls = ProcessParser.apiIter(process)

    def test_apiSequence_createsSequenceWithCallsIndexes(self):
        databaseConfiguration = DatabaseConfiguration()
        apiDict = ApiSequence(databaseConfiguration).createSequence(self.apiCalls)
        self.assertEqual(8, len(apiDict))
