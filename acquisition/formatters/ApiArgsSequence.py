from acquisition.persisters.mongodb.ArgsPersister import ArgsPersister
from acquisition.persisters.mongodb.CallsPersister import CallsPersister


class ApiArgsSequence(object):
    def __init__(self, databaseConfiguration):
        self.callsPersister = CallsPersister(databaseConfiguration)
        self.argsPersister = ArgsPersister(databaseConfiguration)

    def createSequence(self, apiCalls):
        """
        :param apiCalls: A list of APIs: The named tuple in JsonReporterReader.
        :return: A time series (ordered list of APIs index in the DB).
        """
        sequence = []
        for call in apiCalls:
            callRepresentation = [self.callsPersister.getCallIndex(call)]
            for arg in call.arguments:
                argIndex = self.argsPersister.getArgIndex(arg)
                callRepresentation.append(argIndex)
            sequence.append(callRepresentation)
        return sequence
