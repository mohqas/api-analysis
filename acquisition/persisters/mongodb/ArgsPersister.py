from configurations.naming.argTable import argKey, counterCollectionIndex
from acquisition.persisters.mongodb.BasePersister import BasePersister


class ArgsPersister(BasePersister):
    def __init__(self, databaseConfiguration):
        super(ArgsPersister, self).__init__(databaseConfiguration.indexesDatabase)
        self.seenArgs = self.db[databaseConfiguration.seenArgsCollection]
        self.counter = self.db[databaseConfiguration.argsCounterCollection]

    @staticmethod
    def _represent_argument(argument):
        # Since the chances in arguments in the report, we receive a tuple of arg_type and arg_value. For now, we use value alone,
        # can be changed to a concatenated representation if needed
        return argument[1]

    def getArgIndex(self, argument):
        """
        :param argument: The API call argument
        :return: The index of that API call argument in the arguments table recorded across multiple analysis
        """
        return self.seenArgs.find_one({argKey: self._represent_argument(argument)})["_id"]

    # Fine as Call, loop on args and do finds/stores per arg (slow!)
    def persistArg(self, argument):
        """
        Checks if the API is already in the data base. If it is, returns. Otherwise, persist it with a new index in the table.
        :param argument: The API call argument
        :return: None
        """
        if self.seenArgs.find({argKey: self._represent_argument(argument)}).limit(1).count() == 1:
            return
        self._insertIncrementId(self._represent_argument(argument), self.seenArgs)

    def getTotalNumberOfPersistedArgs(self):
        """
        :return: The number of already persisted Arguments in the table. This is used to calculate the dimensionality when switching
        series to a sparse continuous representation.
        """
        return self.seenArgs.count()

    def _insertIncrementId(self, representedArgument, seenArgsCollection):
        # If counter collection doesn't exist, create it.
        # TODO might break since the .name of collection returns a unicode. If so, decode it.
        try:
            argId = self.counter.find_and_modify(
                query={"collection": self.seenArgs.name},
                update={'$inc': {counterCollectionIndex: 1}},
                fields={counterCollectionIndex: 1, "_id": 0},
                new=True
            ).get(counterCollectionIndex)
        except AttributeError:
            self.counter.insert({"collection": self.seenArgs.name, counterCollectionIndex: 1})
            argId = 1

        seenArgsCollection.insert({"_id": argId, argKey: representedArgument})
