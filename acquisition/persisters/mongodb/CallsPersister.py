from configurations.naming.apisTable import apiKey, counterCollectionIndex
from acquisition.persisters.mongodb.BasePersister import BasePersister


class CallsPersister(BasePersister):
    def __init__(self, databaseConfiguration):
        super(CallsPersister, self).__init__(databaseConfiguration.indexesDatabase)
        self.seenAPIs = self.db[databaseConfiguration.seenAPIsCollection]
        self.counter = self.db[databaseConfiguration.apiCounterCollection]

    def getCallIndex(self, apiCall):
        """
        :param apiCall: The API call details (name, parameters, time, return value, status, threadId)
        :return: The index of that API in the APIs table recorded across multiple analysis
        """
        return self.seenAPIs.find_one({apiKey: apiCall.api})["_id"]

    def persistCall(self, apiCall):
        """
        Checks if the API is already in the data base. If it is, returns. Otherwise, persist it with a new index in the table.
        :param apiCall: The API call details (name, parameters, time, return value, status, threadId)
        :return: None
        """
        if self.seenAPIs.find({apiKey: apiCall.api}).limit(1).count() == 1:
            return
        self._insertIncrementId(apiCall.api, self.seenAPIs)

    def getTotalNumberOfPersistedCalls(self):
        """
        :return: The number of already persisted APIs in the table. This is used to calculate the dimensionality when switching series to
        a sparse continuous representation.
        """
        return self.seenAPIs.count()

    def _insertIncrementId(self, api, seenApisCollection):
        # If counter collection doesn't exist, create it.
        # TODO might break since the .name of collection returns a unicode. If so, decode it.
        try:
            apiId = self.counter.find_and_modify(
                query={"collection": self.seenAPIs.name},
                update={'$inc': {counterCollectionIndex: 1}},
                fields={counterCollectionIndex: 1, "_id": 0},
                new=True
            ).get(counterCollectionIndex)
        except AttributeError:
            self.counter.insert({"collection": self.seenAPIs.name, counterCollectionIndex: 1})
            apiId = 1

        seenApisCollection.insert({"_id": apiId, apiKey: api})
