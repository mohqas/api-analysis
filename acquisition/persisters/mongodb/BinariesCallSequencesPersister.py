from configurations.naming.processDict import processName, iD, md5Sum, processApiSequence, processFamily, familyVersion
from acquisition.persisters.mongodb.BasePersister import BasePersister


class BinariesCallSequencesPersister(BasePersister):
    def __init__(self,  databaseConfiguration):
        self.limitOnSeriesLength = databaseConfiguration.hardLimit
        super(BinariesCallSequencesPersister, self).__init__(databaseConfiguration.analysisDatabase)
        self.processSequencesCollection = self.db[databaseConfiguration.processSequencesCollection]
        self.processSequenceWithArgsCollection = self.db[databaseConfiguration.processSequenceWithArgsCollection]

    def getAllApiSequences(self, samplesFilter=None):
        """
        This method is used to get API sequences of already analysed reports from Cuckoo.
        :param samplesFilter: An optional filter for which series to get (e.g specific family or version), default: retrieve all
        :return: returns an iterator of stored API sequence in the current analysis database. The dictionary of each API series contains
        information about the process, the analysed binary, and the series.
        """
        if not samplesFilter:
            samplesFilter = {}

        return self.processSequencesCollection.find(samplesFilter)

    def persistProcessWithArgsSequence(self, process, sequence, category, version):
        """
        This method stores an API sequence in the analysis database. The method is used when analysing JSON reports from Cuckoo and
        persisting the processes within. It checks if the process has already been stored, and stores it otherwise.
        :param process: process document
        :param sequence: Iterator of numbers (indexes of APIs)
        :param category: Malware family/benign
        :param version: Version in family
        :return: None
        """
        self._persistSequence(self.processSequenceWithArgsCollection, process, sequence, category, version)

    def persistProcessSequence(self, process, sequence, category, version):
        """
        This method stores an API sequence in the analysis database. The method is used when analysing JSON reports from Cuckoo and
        persisting the processes within. It checks if the process has already been stored, and stores it otherwise.
        :param process: process document
        :param sequence: Iterator of numbers (indexes of APIs)
        :param category: Malware family/benign
        :param version: Version in family
        :return: None
        """
        self._persistSequence(self.processSequencesCollection, process, sequence, category, version)

    def _persistSequence(self, collection, processInfo, sequence, category, version):
        cutSequence = sequence[:self.limitOnSeriesLength]
        if self._checkProcessDbSanity(collection, processInfo, cutSequence, category):
            collection.insert(self._createProcessDocument(category, processInfo, cutSequence, version))
            self.logger.info("Process: %s persisted correctly.", processInfo.processName)
            return
        self.logger.info("Process: %s was not persisted.", processInfo.processName)

    @staticmethod
    def _createProcessDocument(category, process, series, version):
        return {processName: process.processName, iD: process.processId, md5Sum: process.binaryMd5,
                processApiSequence: series, processFamily: category, familyVersion: version}

    def _checkProcessDbSanity(self, collection, process, series, category):
        query = {processName: process.processName, md5Sum: process.binaryMd5, processApiSequence: series}
        count = collection.find(query).count()
        # Doesn't exist
        if count == 0:
            return True
        # Exist once
        if count == 1:
            self.logger.info("Process already exists.")
            existingCategory = collection.find(query)[0]["category"]
            if existingCategory != category:
                if existingCategory == "unknown":
                    collection.find_one_and_update(query, {"$set": {"category": category}})
                    self.logger("However, category has been updated.")
            return False
        # Exists many times:
        self.logger.warning("Following process exists many times: \n Process: %s \n Series: %s \n", process, series)
        return False
