import pymongo

from configurations.Logger import getLogger


class BasePersister(object):
    def __init__(self, database):
        client = pymongo.MongoClient(socketKeepAlive=True)
        self.db = client[database]
        self.logger = getLogger()





