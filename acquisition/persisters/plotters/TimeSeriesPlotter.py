import os

from matplotlib import pyplot as plt

from configurations.naming.processDict import processApiSequence, processFamily, familyVersion, iD, md5Sum, processName


class PlotApiSequence(object):
    def __init__(self, plotsLocation):
        self.plotsLocation = plotsLocation
        self._ensureDirectory(plotsLocation)

    def plotProcess(self, processDict):
        description = self._getPlotDesc(processDict)
        figureName = "-".join([processDict[processFamily], processDict[familyVersion], str(processDict[iD])])
        self.plotSequence(description, figureName, processDict[processApiSequence])

    @staticmethod
    def _getPlotDesc(processDict):
        description = ["Graph Info:", "  - MD5: %s" % processDict[md5Sum], "  - Process: %s" % processDict[processName],
                       "  - Family: %s" % processDict[processFamily], "  - Version: %s" % processDict[familyVersion]]

        return "\n".join(description)

    @staticmethod
    def _ensureDirectory(directory):
        if not os.path.exists(directory):
            os.makedirs(directory)

    def plotSequence(self, desc, figureName, series):
        fig = plt.figure(figsize=(16, 6))
        ax0 = fig.add_subplot(121)
        ax0.plot(series, '.')
        ax0.set_xlabel("Time step")
        ax0.set_ylabel("API index")
        ax1 = fig.add_subplot(122)
        ax1.axes.get_xaxis().set_visible(False)
        ax1.axes.get_yaxis().set_visible(False)
        ax1.text(0.05, 0.95, desc, horizontalalignment='left', verticalalignment='top')
        plt.tight_layout()
        plt.savefig("%s/%s.png" % (self.plotsLocation, figureName))
        plt.clf()
        plt.close(fig)
