import os
from unittest import TestCase
from acquisition.CuckooJsonMediator import CuckooJsonMediator
from configurations.database import DatabaseConfiguration
from pymongo import MongoClient
from configurations.naming.processDict import processApiSequence

TESTFILE = os.path.join(os.path.dirname(__file__), "testData/report.json")


class TestDbConfig(DatabaseConfiguration):
    analysisDatabase = "test-analysis"
    indexesDatabase = "test-index"


class TestPlotsConfiguration(object):
    resultsLocation = "/tmp/"


class TestCuckooJsonMediator(TestCase):
    def setUp(self):
        self.mc = MongoClient()
        self._cleanDbs()
        self.mediator = CuckooJsonMediator(TestDbConfig(), TestPlotsConfiguration())

    def test_readingReportMultipleTimes_resultsTheSameSequence(self):
        # TODO: The test should be carried with API objects and the indexing should be done through the mediator
        # TODO: Test does not show what is failing when it does. Could be written better
        expectedSequence = [1, 2, 1, 3, 2, 4, 5, 6]
        testResult = True

        for i in range(9):
            self.mediator.processSequencesOfReport(reportPath=TESTFILE, binaryFamily="test", familyVersion="1.0")
            testResult &= self._getResultSequence() == expectedSequence

        self.assertTrue(testResult)

    def _cleanDbs(self):
        dbConfig = TestDbConfig()
        self.mc.drop_database(dbConfig.analysisDatabase)
        self.mc.drop_database(dbConfig.indexesDatabase)

    def _getResultSequence(self):
        dbConfig = TestDbConfig()
        db = self.mc[dbConfig.analysisDatabase]
        collection = db[dbConfig.processSequencesCollection]
        return collection.find_one()[processApiSequence]
