# TODO: Check cuckoo flag when getting binaries
import os

from configurations.inspector import InspectorConfiguration
from configurations.database import DatabaseConfiguration
from configurations.results import ResultsConfiguration
from configurations.Logger import getLogger
from submitter.persisters.mongodb.SubmissionsPersister import SubmissionsPersister
from processCuckooReport import applyPolicy as applyCuckooPolicy


def main(dbConfiguration, inspectConfiguration, plotsConfig):
    persister = SubmissionsPersister(dbConfiguration)
#    notAnalyzed = persister.getNotAnalyzedBinaries(sandboxName=CuckooConfiguration().name)
    notAnalyzed = persister.getNotAnalyzedBinaries()
    logger = getLogger()

    logger.info("Found %d binaries to analyse", len(notAnalyzed))
    for binary in notAnalyzed:
        iD = binary["_id"]
        report = inspectConfiguration.cuckooReportsPathForId % iD
        logger.info("Processing binary ID %s, report: %s", iD, report)

        if os.path.exists(report):
            applyCuckooPolicy(dbConfiguration, plotsConfig, report, binary["category"], binary["version"])
            persister.setBinaryAnalyzed(iD)
        else:
            logger.info("Report not found. Maybe analysis were not run yet. Execute Cuckoo sandbox")


if __name__ == "__main__":
    # TODO pass configurations as script arguments!
    databaseConfiguration = DatabaseConfiguration()
    plotConfig = ResultsConfiguration()
    inspectorConfiguration = InspectorConfiguration()

    main(databaseConfiguration, inspectorConfiguration, plotConfig)
