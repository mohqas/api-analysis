from acquisition.CuckooJsonMediator import CuckooJsonMediator
from configurations.database import DatabaseConfiguration
from configurations.results import ResultsConfiguration
import argparse


def applyPolicy(dbConfigs, plotsConfigs, filePath, category, version):
    policyMediator = CuckooJsonMediator(dbConfigs, plotsConfigs)
    policyMediator.processSequencesOfReport(reportPath=filePath, binaryFamily=category, familyVersion=version)


def parseArguments():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('fn', metavar='filename', type=str,
                        help='the report filepath to run the analysis on')
    parser.add_argument('category', nargs="?", default='unknown',
                        help='the classification for the executable (unknown -default-, benign, harmful or its family)')
    parser.add_argument("version", nargs="?", default="unknown",
                        help="The version of the executable family if the family is used as a category")
    return parser.parse_args()


if __name__ == "__main__":
    args = parseArguments()
    applyPolicy(DatabaseConfiguration(),
                ResultsConfiguration(),
                args.fn, args.category, args.version)

