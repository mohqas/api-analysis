# These are the naming conventions in the cuckoo report. Since they already changed twice, better have them configurable
targetInfo = "target"
fileInfo = "file"
nameField = "name"
md5Field = "md5"
behaviorInfo = "behavior"
processesDetails = "processes"
parentId = "ppid"
processId = "pid"
processName = "process_name"
firstSeen = "first_seen"
callsList = "calls"

# These are API specific info fields
callCategory = "category"
callStatus = "status"
callReturnValue = "return"
callTimestamp = "timestamp"
threadId = "threadId"
isRepeated = "repeated"
apiName = "api"
apiArguments = "arguments"