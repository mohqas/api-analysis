collectionIndex = "_id"  # Fixed! We are using the internal MongoDb index
submittedBinaryCategory = "category"  # Family/Type
binaryFamilyVersion = "version"  # in case the category was a malware family
isAnalyzed = "analyzed"
sandbox = "sandbox"
