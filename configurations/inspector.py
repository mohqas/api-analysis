
class InspectorConfiguration(object):
    """
    This class contains configurations related to the inspector script, which analysis the resulting reports from Cuckoo for a given
    analysis run (database). The results are stored in the same database where the analysis IDs are submitted (see database
    configuration: processSeriesCollection vs binaryAnalysisIdCollection)
    The cuckoo reports path format variable contains what the path of a report looks like for a given analysis Id. It contains the path
    to the analysis storage of cuckoo followed by the internal structure for a given analysis Id.
    """
    cuckooReportsPathForId = "/path/to/cuck/reports/%s/reports/report.json"
    apiInspectMainPath = "/path/to/processCuckooReport.py"
