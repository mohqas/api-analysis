
class SubmitterConfiguration(object):
    """
    This class contains configurations related to the execBinAndPersistId script, which runs a binary analysis request in Cuckoo and
    persist the binary path and analysis ID in the data base for ApiInspect to analyze later.
    """
    cuckooSubmitBinaryPath = ""
    analysisTimeout = 150  # In seconds!
