
class DatabaseConfiguration(object):
    """
    This class contains configurations related to databases. Some of these configurations should remain constant unless necessary!
    The following are the parameters:

    Configurable:
    :var AnalysisDatabase: The name of the database in which series of the current analysis are stored. This database is also used by the
    inspector to read series for analysis.

    Better remain constant
    :var ProcessSeriesCollection: Stores the collection name in the Analysis database, in which API series are stored
    :var BinariesAnalysisID: used by the submitter method. It stores:
     - The path of the binary to analyse by Cuckoo
     - A flag stating whether the resulting report from Cuckoo was analyzed
    :var HardLimit: A hard limit on the maximum size fo the series to store (avoiding BSON object size error)

    :var APIsDatabase: The database containing the table of APIs and their index numbers
    :var CountersCollection: Is a helper collection in the APIsDatabase to keep the current index for the APIs table
    :var SeenAPIsCollection: The table of seen APIs in the analysis
    """

    # Old configuration
    # Used for database name and the output plots
    analysisDatabase = "full"
    isVersionDatabase = True
    processSequencesCollection = "api_sequences"
    processSequenceWithArgsCollection = "api_with_args_sequences"
    hardLimit = 6000
    binariesAnalysisIDCollection = "submitted_binaries"

    indexesDatabase = "analysis_index"
    apiCounterCollection = "api_counter"
    argsCounterCollection = "args_counter"
    seenAPIsCollection = "seen_calls"
    seenArgsCollection = "seen_args"

