
class CuckooConfiguration(object):
    """
    This class contains configurations related to the Cuckoo sandbox script
    For now, only the time is set. Other configuration might be added later
    """
    name = "Cuckoo"
    cuckooSubmitBinaryPath = "/path/to/cuckoo/utils/submit.py"
    analysisTimeout = 120  # In seconds!
