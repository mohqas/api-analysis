import logging

LOGGER = None


class Logger(object):
    def __init__(self):
        self._logger = logging.getLogger("apinspect")
        self._logger.setLevel(logging.DEBUG)

        handler = logging.StreamHandler()
        handler.setLevel(logging.INFO)
        self._logger.addHandler(handler)


def getLogger():
    global LOGGER
    if not LOGGER:
        LOGGER = Logger()._logger
    return LOGGER
