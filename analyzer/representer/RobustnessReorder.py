import random
from copy import deepcopy

from analyzer.representer.Representer import Representer


class RobustnessReorder(Representer):
    def __init__(self, apiLimit, reorderWindowSie, changePercentage):
        self.apiLimit = apiLimit
        self.reorderWindowSize = reorderWindowSie
        self.changePercentage = changePercentage
        self.name = "API reorder"

    def transform(self, sequencesDict):
        transformedData = {}
        for category in sequencesDict:
            transformedData[category] = map(self._reorderCalls, sequencesDict[category])
        return transformedData

    def _reorderCalls(self, processInfo):
        numberOfReorders = (self.apiLimit * self.changePercentage * 1.0) / (100.0 * self.reorderWindowSize)
        sequence = deepcopy(processInfo.sequence)
        # Do the magic
        alreadyDoneIndexes = []
        for i in range(int(numberOfReorders)):
            index = random.randrange(0, self.apiLimit - self.reorderWindowSize)
            while index in alreadyDoneIndexes:
                index = random.randrange(0, self.apiLimit - self.reorderWindowSize)
            alreadyDoneIndexes.append(index)
            toShuffle = sequence[index:index + self.reorderWindowSize]
            random.shuffle(toShuffle)
            sequence[index:index + self.reorderWindowSize] = toShuffle
        return processInfo._replace(sequence=sequence)
