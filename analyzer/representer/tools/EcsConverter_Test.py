from unittest import TestCase
from analyzer.representer.tools.EcsConverter import EcsConverter
from configurations.naming.processDict import md5Sum, processFamily


testApiSequence= {"process": 1, "ID": 1, "md5": "some MD5 sum",
                  "sequence": [1, 4, 4, 6, 6, 8, 10, 12], "category": "citadel", "version": "1.1"}


class TestEcsConverter(TestCase):
    def test_embedSequence_createsASequenceThatIs_dimensionalityMultiplesNumberOfWindows(self):
        testSequence = [1, 2, 3, 4, 5]
        yAxisLen = max(testSequence)
        xAxisWindow = 3

        expandedSequence = EcsConverter(yAxisLen, xAxisWindow, regularize=False).embedSequence(testSequence)
        self.assertEqual(len(expandedSequence), 10)

    def test_embedSequence_correctlyCountsElements(self):
        testSequence = [1, 2, 3, 4, 5]
        yAxisLen = max(testSequence)
        xAxisWindow = 3
        expectedSequence = [1, 1, 1, 0, 0, 0, 0, 0, 1, 1]

        self._assertExpandedAndExpectedAreEqualForWindowAndDimensionality(expectedSequence, testSequence, xAxisWindow, yAxisLen)

        testSequence = [1, 1, 1, 1, 5]
        yAxisLen = max(testSequence)
        xAxisWindow = 3
        expectedSequence = [3, 0, 0, 0, 0, 1, 0, 0, 0, 1]

        self._assertExpandedAndExpectedAreEqualForWindowAndDimensionality(expectedSequence, testSequence, xAxisWindow, yAxisLen)

        testSequence = [1, 1, 1, 1, 2]
        yAxisLen = max(testSequence)
        xAxisWindow = 1
        expectedSequence = [1., 0., 1., 0., 1., 0., 1., 0., 0., 1.]

        self._assertExpandedAndExpectedAreEqualForWindowAndDimensionality(expectedSequence, testSequence, xAxisWindow, yAxisLen)

    def test_embedSequence_correctlyCountsElements_andRegularizeThemIfFlagIsPassed(self):
        testSequence = [1, 2, 3, 4, 5]
        yAxisLen = max(testSequence)
        xAxisWindow = 3
        expectedSequence = [0.2, 0.2, 0.2, 0, 0, 0, 0, 0, 0.2, 0.2]

        self._assertExpandedAndExpectedAreEqual_forWindowAndDimensionality_withRegularization(expectedSequence, testSequence,
                                                                                              xAxisWindow, yAxisLen)

        testSequence = [1, 1, 1, 1, 5]
        yAxisLen = max(testSequence)
        xAxisWindow = 3
        expectedSequence = [0.6, 0, 0, 0, 0, 0.2, 0, 0, 0, 0.2]

        self._assertExpandedAndExpectedAreEqual_forWindowAndDimensionality_withRegularization(expectedSequence, testSequence, xAxisWindow,
                                                                                              yAxisLen)

        testSequence = [1, 1, 1, 1, 2]
        yAxisLen = max(testSequence)
        xAxisWindow = 1
        expectedSequence = [0.2, 0., 0.2, 0., 0.2, 0., 0.2, 0., 0., 0.2]

        self._assertExpandedAndExpectedAreEqual_forWindowAndDimensionality_withRegularization(expectedSequence, testSequence, xAxisWindow,
                                                                                              yAxisLen)

#   def test_createProcessEmbeddedSequence_keepsInputProcessInfoAsTheyAre(self):
#        output = EcsConverter(2, 12, regularize=False).createProcessEmbeddedSequence(testApiSequence)
#        self.assertEqual(output[md5Sum], testApiSequence[md5Sum])
#        self.assertEqual(output[processFamily], testApiSequence[processFamily])

    def _assertExpandedAndExpectedAreEqualForWindowAndDimensionality(self, expectedSequence, testSequence, xAxisWindow, yAxisLen):
        expandedSequence = EcsConverter(yAxisLen, xAxisWindow, regularize=False).embedSequence(testSequence)
        self.assertEqual(expandedSequence, expectedSequence)

    def _assertExpandedAndExpectedAreEqual_forWindowAndDimensionality_withRegularization(self, expectedSequence, testSequence, xAxisWindow,
                                                                                         yAxisLen):
        expandedSequence = EcsConverter(yAxisLen, xAxisWindow, regularize=True).embedSequence(testSequence)
        self.assertEqual(expandedSequence, expectedSequence)
