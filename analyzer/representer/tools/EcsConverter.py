from copy import deepcopy
from collections import Counter

from configurations.naming.processDict import processApiSequence


class EcsConverter(object):
    def __init__(self, dimensions, window, regularize):
        self.window = window
        self.dimensionality = dimensions
        self.regularize=regularize

#    def createProcessEmbeddedSequence(self, processDocument):
#        output = deepcopy(processDocument)
#        output[processApiSequence] = self.embedSequence(output[processApiSequence])
#        return output

    def embedSequence(self, categoricalSequence):
        if self.regularize:
            regularizationFactor = len(categoricalSequence)
        else:
            regularizationFactor = 1

        output = []
        i = 0
        categoriesCounter = Counter()

        for item in categoricalSequence:
            i += 1
            categoriesCounter[item] += 1
            if self._windowIsFinished(i):
                self._updateOutput(categoriesCounter, output, regularizationFactor)
                categoriesCounter.clear()
        # For the last window in case there are scanned elements
        if not self._windowIsFinished(i):
            self._updateOutput(categoriesCounter, output, regularizationFactor)

        return map(float, output)

    def _windowIsFinished(self, i):
        windowFinished = i % self.window == 0
        return windowFinished

    def _updateOutput(self, categoriesCounter, output, regularizationFactor):
        for ind in range(self.dimensionality):
            item = ind + 1
            output.append(categoriesCounter[item]*1.0/regularizationFactor)
