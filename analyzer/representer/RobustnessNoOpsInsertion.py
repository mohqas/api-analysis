from copy import deepcopy
import random

from analyzer.representer.Representer import Representer


class RobustnessNoOpsInsertion(Representer):
    def __init__(self, apiLimit, apiIndexSize, changePercentage):
        self.apiLimit = apiLimit
        self.changePercentage = changePercentage
        self.indexSize = apiIndexSize
        self.name = "NoOp API Insertion"

    def transform(self, sequencesDict):
        transformedData = {}
        for category in sequencesDict:
            transformedData[category] = map(self._insertNoOps, sequencesDict[category])
        return transformedData

    def _insertNoOps(self, processInfo):
        numberOfNoOpsToInsert = (self.apiLimit * self.changePercentage * 1.0) / 100.0
        sequence = deepcopy(processInfo.sequence)
        alreadyDoneIndexes = []

        for i in range(int(numberOfNoOpsToInsert)):
            index = random.randrange(0, self.apiLimit - numberOfNoOpsToInsert)
            while index in alreadyDoneIndexes:
                index = random.randrange(0, self.apiLimit - numberOfNoOpsToInsert)
            alreadyDoneIndexes.append(index)
            sequence.insert(index, random.randrange(self.indexSize))
        sequence = sequence[:self.apiLimit]
        return processInfo._replace(sequence=sequence)

