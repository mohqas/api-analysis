import random
from copy import deepcopy

from analyzer.representer.Representer import Representer


class RobustnessRandomReplace(Representer):
    def __init__(self, apiLimit, apiIndexSize, changePercentage):
        self.apiLimit = apiLimit
        self.changePercentage = changePercentage
        self.indexSize = apiIndexSize
        self.name = "Random API Replace"

    def transform(self, sequencesDict):
        transformedData = {}
        for category in sequencesDict:
            transformedData[category] = map(self._randomReplace, sequencesDict[category])
        return transformedData

    def _randomReplace(self, processInfo):
        elementsToReplace = (self.apiLimit * self.changePercentage * 1.0) / 100.0
        sequence = deepcopy(processInfo.sequence)
        # do magic!
        alreadyDoneIndexes = []
        for i in range(int(elementsToReplace)):
            index = random.randrange(0, self.apiLimit)
            while index in alreadyDoneIndexes:
                index = random.randrange(0, self.apiLimit)
            alreadyDoneIndexes.append(index)
            sequence[index] = random.randrange(self.indexSize)
        return processInfo._replace(sequence=sequence)
