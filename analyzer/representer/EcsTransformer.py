from collections import defaultdict
from analyzer.representer.Representer import Representer
from analyzer.representer.tools.EcsConverter import EcsConverter


class EcsTransformer(Representer):
    def __init__(self, dimensions, window, regularize=False):
        self.converter = EcsConverter(dimensions, window, regularize)

    def transform(self, sequencesDict):
        output = defaultdict(list)

        for category in sequencesDict:
            for processInfo in sequencesDict[category]:
                output[category].append(self._convertSequence(processInfo))
        return output

    def _convertSequence(self, processInfo):
        return processInfo._replace(sequence=self.converter.embedSequence(processInfo.sequence))

