import unittest
from analyzer.retriever.MongoDbSequences import ProcessInfo

# Design change ==> Apply ECS converter on dictionary instead of process document. Process documents are dropper now at the acquisition
# and only perfamily/perversion dicts come out.
from analyzer.representer.EcsTransformer import EcsTransformer

sequencesDict = {
    "mal_family": [ProcessInfo(sequence=[1, 4, 4], name="process.exe"),
                   ProcessInfo(sequence=[4, 4, 1, 3, 3], name="askdja")]
}

expectedDict = {
    "mal_family": [ProcessInfo(sequence=[1, 0, 0, 2], name="process.exe"),
                   ProcessInfo(sequence=[1, 0, 2, 2], name="askdja")]
}

expectedDictRegularized = {
    "mal_family": [ProcessInfo(sequence=[1./3, 0, 0, 2./3], name="process.exe"),
                   ProcessInfo(sequence=[1./5, 0, 2./5, 2./5], name="askdja")]
}

class EscTransformer_Test(unittest.TestCase):
    def test_transformer_keepsProcessInfoIntact(self):
        transformer = EcsTransformer(4, 10)

        self.assertEqual(transformer.transform(sequencesDict), expectedDict)

    def test_transformer_regularizeCorrectly(self):
        transformer = EcsTransformer(4, 10, regularize=True)

        self.assertEqual(transformer.transform(sequencesDict), expectedDictRegularized)