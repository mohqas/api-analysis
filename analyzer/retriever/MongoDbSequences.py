# TODO: Cleanup and add tests ! (mock the self._testing/training sequences)
import random
from math import ceil
import numpy as np

from collections import defaultdict, namedtuple
from acquisition.persisters.mongodb.BinariesCallSequencesPersister import BinariesCallSequencesPersister
from acquisition.persisters.mongodb.CallsPersister import CallsPersister
from configurations.Logger import getLogger
from configurations.database import DatabaseConfiguration
from configurations.naming.processDict import processFamily, processApiSequence, familyVersion, processName


WhiteListProcesses = ["explorer.exe", "taskhost.exe", "Reader_sl.exe", "wmpnscfg.exe", "cmd.exe", "wmplayer.exe", "soffice.bin",
                      "svchost.exe"]

ProcessInfo = namedtuple("ProcessInfo", "sequence name")


class MongoDbSequences(object):
    def __init__(self, minimumApiCount, perVersion=False, trainingPercentage=10, ignoredApis=0, perFamilyAndVersion=False,
                 samplesFilter=None):
        if perFamilyAndVersion and perVersion:
            raise RuntimeError("Can't process data for both perVersion and perFamilyAndVersion flags. Choose one of both or none.")

        databaseConfiguration = DatabaseConfiguration()
        self.persister = BinariesCallSequencesPersister(databaseConfiguration)
        self.callsPersister = CallsPersister(databaseConfiguration)
        self.minimumAPICount = minimumApiCount
        self.ignoresAPIs = ignoredApis
        self.trainingPercentage = trainingPercentage
        self.perVersion = perVersion
        self.perFamilyAndVersion = perFamilyAndVersion
        self.samplesFilter = samplesFilter
        self._trainingSequences = []
        self._testingSequences = []
        self.logger = getLogger()

    def _cutSequenceAtThresholds(self, sequence):
        sequence[processApiSequence] = sequence[processApiSequence][self.ignoresAPIs: self.minimumAPICount]
        return sequence

    # Add a per family and version flag and its processing
    def _getAllSequences(self):
        sequences = self.persister.getAllApiSequences(samplesFilter=self.samplesFilter)

        sequencesFixed = np.array([self._cutSequenceAtThresholds(s) for s in sequences if len(s[processApiSequence]) > self.minimumAPICount
                                   and s[processName] not in WhiteListProcesses])

        if len(sequencesFixed) == 0:
            raise RuntimeError("No sequences found. Check the used samples filter and database configuration")

        # The intermediate dictionary is created to insure that there is a percentage of each category in training and testing data sets
        sequencesDict = defaultdict(list)
        for seq in sequencesFixed:
            if self.perFamilyAndVersion:
                sequencesDict[self.__getFamilyAndVersionDictKeyFromSequence(seq)].append(seq)
            elif self.perVersion:
                sequencesDict[seq[familyVersion]].append(seq)
            else:
                sequencesDict[seq[processFamily]].append(seq)

#       self.logger.info("Retrieved the following sequences dict from the database")
#       for category in sequencesDict:
#           self.logger.info("Category: %s, sequences count: %s" % (category, len(sequencesDict[category])))

        for category in sequencesDict:
            if len(sequencesDict[category]) > 10:
                random.shuffle(sequencesDict[category])
                testSequencesCount = int(ceil(len(sequencesDict[category]) * 10. / 100))
                self._trainingSequences.extend(sequencesDict[category][:-testSequencesCount/2])
                self._testingSequences.extend(sequencesDict[category][-testSequencesCount:])

    def acquireTrainingSequencesDictionary(self):
        if not self._trainingSequences:
            self._getAllSequences()
        return self._retrieveSequencesDict(self._trainingSequences)

    def acquireTestSequencesDictionary(self):
        if not self._testingSequences:
            self._getAllSequences()
        return self._retrieveSequencesDict(self._testingSequences)

    def acquireAllSequencesDictionary(self):
        if not self._testingSequences or not self._trainingSequences:
            self._getAllSequences()
        return self._retrieveSequencesDict(self._testingSequences + self._trainingSequences)

    def _retrieveSequencesDict(self, sequences):
        if self.perFamilyAndVersion:
            return self._getSequencesOfProcessInfoPerKeyDict(sequences, keyMethod=self.__getFamilyAndVersionDictKeyFromSequence)
        elif self.perVersion:
            return self._getSequencesOfProcessInfoPerKeyDict(sequences, keyMethod=lambda seq: seq[familyVersion])
        return self._getSequencesOfProcessInfoPerKeyDict(sequences, keyMethod=lambda seq: seq[processFamily])

    @staticmethod
    def _getSequencesOfProcessInfoPerKeyDict(sequencesList, keyMethod):
        sequencesDict = defaultdict(list)
        for s in sequencesList:
            sequencesDict[keyMethod(s)].append(ProcessInfo(sequence=s[processApiSequence], name=str(s[processName])))

        return sequencesDict

    @staticmethod
    def __getFamilyAndVersionDictKeyFromSequence(seq):
        return "%s %s" % (seq[processFamily], seq[familyVersion])
