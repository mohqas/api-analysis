import os
from collections import defaultdict
from analyzer.visualizers.EvaluationBarsPlotter import EvaluationBarsPlotter
from analyzer.visualizers.ResultsPersister import ResultsPersister

from configurations.results import ResultsConfiguration
from configurations.database import DatabaseConfiguration
from analyzer.processes.Classifier import ClfClassifier, LinearSVMClassifier, NonLinearSVMClassifier, RFClassifier, EnsembleVoting
from analyzer.representer.EcsTransformer import EcsTransformer
from analyzer.visualizers.AccuracyAcrossWindowsPlot import AccuracyAcrossWindowsPlot
from analyzer.visualizers.AccuracyMatrixPlot import AccuracyMatrixPlot
from analyzer.visualizers.TextVisualizer import TextVisualizer
from analyzer.retriever.MongoDbSequences import MongoDbSequences
from acquisition.persisters.mongodb.CallsPersister import CallsPersister

ApiCountToConsider = 400
Classifiers = [LinearSVMClassifier, NonLinearSVMClassifier, RFClassifier, EnsembleVoting]
SingleVisualizers = [TextVisualizer, AccuracyMatrixPlot]
MultiVisualizers = [ResultsPersister, AccuracyAcrossWindowsPlot, EvaluationBarsPlotter]
TestWindows = [10, 65, 130, 260, 500]


class CuckooClassifierAnalyzer(object):
    def __init__(self, datasetName, apisToConsider, window, classifyPerVersion, outDir):
        self.versions = classifyPerVersion
        self.window = window
        self.datasetName = datasetName
        self.apisToConsider = apisToConsider
        self.sequencesRetriever = MongoDbSequences(minimumApiCount=self.apisToConsider, perVersion=self.versions)

        self.outDir = outDir

    def _acquireTraining(self):
        return self.sequencesRetriever.acquireTrainingSequencesDictionary()

    def _acquireTesting(self):
        return self.sequencesRetriever.acquireTestSequencesDictionary()

    def _acquireAllData(self):
        dictionary = self.sequencesRetriever.acquireAllSequencesDictionary()
        f = open(os.path.join(self.outDir, "stats"), 'w')
        f.write("Statistics for used data: \n")
        for c in dictionary:
            f.write("Category: %s, count: %s \n" % (c, len(dictionary[c])))
        f.close()
        return dictionary

    def _represent(self, sequencesDict):
        totalNumberOfApis = CallsPersister(DatabaseConfiguration()).getTotalNumberOfPersistedCalls()
        return EcsTransformer(totalNumberOfApis, self.window).transform(sequencesDict)

    @staticmethod
    def _train(classifiers, trainingSequences):
        for classifier in classifiers:
            classifier.train(trainingSequences)

    @staticmethod
    def testPolicy(classifiers, testingSequences):
        # type: (Classifier, Dict) -> Dict
        results = {}
        for classifier in classifiers:
            results[classifier.name] = classifier.test(testingSequences)

        return results

    @staticmethod
    def crossValidatePolicy(classifiers, allSequences):
        # type: (Classifier, Dict) -> Dict
        results = {}
        for classifier in classifiers:
            results[classifier.name] = classifier.crossValidate(allSequences)

        return results

    def visualize(self, results, singleVisualizers):
        for visualizer in singleVisualizers:
            for c in results:
                v = visualizer(self.outDir, c, self.datasetName, self.window)
                v.createOutput(results[c].accuracyInfo, results[c].accuracyMatrix)

    def applyTestPolicyAndRetrieveResults(self, classifiers):
        trainingEcses = self._represent(self._acquireTraining())
        testingEcses = self._represent(self._acquireTesting())

        self._train(classifiers, trainingEcses)
        return self.testPolicy(classifiers, testingEcses)

    def applyCrossValidatePolicy(self, classifiers):
        embeddedSequences = self._represent(self._acquireAllData())

        return self.crossValidatePolicy(classifiers, embeddedSequences)

    def persist(self, classifiers):
        for c in classifiers:
            c.save(os.path.join(self.outDir, "%s_%s" % (c.name, self.window)))


def _crossValidationResults(datasetName, outputsDir, resultsPerClassifierPerWindow, version):
    # Review cross validation testing
    for window in TestWindows:
        print "Processing window size: %s" % window
        classifiers = [c() for c in Classifiers]
        facade = CuckooClassifierAnalyzer(datasetName, ApiCountToConsider, window, version, outputsDir)

        results = facade.applyCrossValidatePolicy(classifiers)

        for c in classifiers:
            resultsPerClassifierPerWindow[c.name][window] = results[c.name]

        # Plotting
        v = AccuracyAcrossWindowsPlot(outputsDir, "%s-%s" % (datasetName, "cross-validation"))
        v.createOutput(resultsPerClassifierPerWindow)


def main(version):
    datasetName = DatabaseConfiguration().analysisDatabase
    resultsPerClassifierPerWindow = defaultdict(dict)
    outputsDir = os.path.join(ResultsConfiguration().resultsLocation, datasetName, "accuracy")
    if not os.path.exists(outputsDir):
        os.makedirs(outputsDir)

    # Old randomized testing
    for window in TestWindows:
        print "Processing window size: %s" % window
        classifiers = [c() for c in Classifiers]
        facade = CuckooClassifierAnalyzer(datasetName, ApiCountToConsider, window, version, outputsDir)

        results = facade.applyTestPolicyAndRetrieveResults(classifiers)

        facade.visualize(results, SingleVisualizers)
        facade.persist(classifiers)
        # Create the per-window dict
        for c in classifiers:
            resultsPerClassifierPerWindow[c.name][window] = results[c.name]

    for visualizer in MultiVisualizers:
        v = visualizer(outputsDir, datasetName)
        v.createOutput(resultsPerClassifierPerWindow)

    # This is hacky solution for doing cross validation
#   _crossValidationResults(datasetName, outputsDir, resultsPerClassifierPerWindow, version)

if __name__ == "__main__":
    main(version=DatabaseConfiguration().isVersionDatabase)

