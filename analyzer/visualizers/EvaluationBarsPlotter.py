from collections import Counter, OrderedDict
import os
import math
from sklearn.metrics import confusion_matrix
from analyzer.visualizers.MultiVisualizer import MultiVisualizer
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.cm as cm

BARSPACING = 0.07
XRANGE = np.arange(0, 8, 2)
# Since we need this order, we are keeping this here, for more metrics, please update the method _calculateScoresForWindow
METRICS = ["Precision", "Recall", "F1", "MCC"]


class EvaluationBarsPlotter(MultiVisualizer):
    def __init__(self, outputDir, additionalInfo):
        super(EvaluationBarsPlotter, self).__init__(outputDir)
        self.additionalInfo = additionalInfo

    def createOutput(self, resultsDict, *args, **kwargs):
        # input is a dictionary: {classifier: window: classifierResults}
        orderedByCountLabels = self._retrieveGtLabelsOrderedByCount(resultsDict)
        for classifier in resultsDict:
            self._plotClassifierResults(resultsDict[classifier], classifier)
            for window in sorted(resultsDict[classifier].keys()):
                confMatrix = confusion_matrix(y_true=resultsDict[classifier][window].gtAndPrLabels.groundTruth,
                                              y_pred=resultsDict[classifier][window].gtAndPrLabels.predictedLabels,
                                              labels=orderedByCountLabels)
                rowSums = np.sum(confMatrix, axis=1)
                columnSums = np.sum(confMatrix, axis=0)
                totalSum = np.sum(confMatrix)
                scoresPerLabelForWindow = OrderedDict()
                for i in range(4):
                    tp = float(confMatrix[i][i])
                    fp = float(rowSums[i] - tp)
                    fn = float(columnSums[i] - tp)
                    tn = float(totalSum - fn - fp - tp)
                    scoresPerLabelForWindow[orderedByCountLabels[i]] = self._calculateScoresForTpTnFpFn(tp, tn, fp, fn)
                self._generateTopLabelsBarGraph(classifier, window, scoresPerLabelForWindow)

    @staticmethod
    def _retrieveGtLabelsOrderedByCount(resultsDict):
        aClassifier = resultsDict.keys()[0]
        aWindow = resultsDict[aClassifier].keys()[0]
        resultsOfAWindow = resultsDict[aClassifier][aWindow]
        gtLabels = resultsOfAWindow.gtAndPrLabels.groundTruth
        cDict = Counter(gtLabels)
        return sorted(cDict, key=lambda k: cDict[k], reverse=True)

    def _plotClassifierResults(self, classificationResultsPerWindow, classifierName):
        scoresPerWindow = self._getScoresPerWindowForClassifier(classificationResultsPerWindow)
        self._generatePlotToFile(classifierName, scoresPerWindow)

    def _getScoresPerWindowForClassifier(self, classificationResultsPerWindow):
        scoresPerWindow = {}
        for w in sorted(classificationResultsPerWindow.keys()):
            scoresPerWindow[w] = self._calculateScoresForWindow(classificationResultsPerWindow[w])
        return scoresPerWindow

    # The representation part
    def _calculateScoresForWindow(self, classificationResultsPerWindow):
        groundTruthLabels = classificationResultsPerWindow.gtAndPrLabels.groundTruth
        predictedLabels = classificationResultsPerWindow.gtAndPrLabels.predictedLabels
        confusionMatrix = confusion_matrix(y_true=groundTruthLabels, y_pred=predictedLabels)

        return self._calculateScoresForConfusionMatrix(confusionMatrix)

    @staticmethod
    def _calculateScoresForTpTnFpFn(TTP, TTN, TFP, TFN):
        precision = TTP / (TTP + TFP)
        recall = TTP / (TTP + TFN)
        if precision + recall > 0:
            f1 = 2 * (precision * recall) / (precision + recall)
        else:
            f1 = 0
        mCC = ((TTP * TTN) - (TFP * TFN)) / math.sqrt((TTP + TFP) * (TTP + TFN) * (TTN + TFP) * (TTN + TFN))
        return [precision, recall, f1, mCC]

    def _calculateScoresForConfusionMatrix(self, confusionMatrix):
        TTP, TFP, TFN, TTN = self._calculateTotalConfusionFromConfusionMatrix(confusionMatrix)
        return self._calculateScoresForTpTnFpFn(TTP, TTN, TFP, TFN)

    @staticmethod
    def _calculateTotalConfusionFromConfusionMatrix(cm):
        TTP = TFP = TFN = TTN = 0
        rowSums = np.sum(cm, axis=1)
        columnSums = np.sum(cm, axis=0)
        totalSum = np.sum(cm)

        for i in range(len(cm)):
            TTP += cm[i][i]
            TFP += rowSums[i] - cm[i][i]
            TFN += columnSums[i] - cm[i][i]
            TTN += totalSum - (rowSums[i] + columnSums[i] - cm[i][i])

        return float(TTP), float(TFP), float(TFN), float(TTN)

    # The plotting part
    def _generatePlotToFile(self, classifierName, scoresPerWindowDict):
        fig = plt.figure(1, figsize=(9, 6))
        ax = fig.add_subplot(111)

        numOfBars = len(METRICS)
        barWidth = (1.5 / numOfBars) - (3 * BARSPACING)

        labels = []
        bars = []
        # group is which bars we are plotting, that would be a window
        hatches = ['.', '+', 'x', '\\', '-', 'o', 'O', '*']

        group = 1

        for window in sorted(scoresPerWindowDict.keys()):
            labels.append(window)
            xLocations = self._calculateXLocations(group, barWidth)
            color = cm.terrain(0.0 + 0.05 * (group - 1))
            values = scoresPerWindowDict[window]
            hatch = hatches[group - 1]
            self._plotGroupBars(ax, xLocations, values, barWidth, color, hatch, bars)
            group += 1

        self._setXYLimits(ax, barWidth)

        self._setXTicks(ax, barWidth)
        title = "Metric Scores for The %s Classifier \n for Different Window Sizes" % classifierName
        self._setLabelsLegend(ax, bars, labels, title)

        plt.savefig("%s.png" % os.path.join(self.outDir, "metrics-%s-%s" % (self.additionalInfo, classifierName)))
        plt.close()
        plt.clf()

    def _generateTopLabelsBarGraph(self, classifierName, windowSize, scoresPerLabel):
        fig = plt.figure(1, figsize=(9, 6))
        ax = fig.add_subplot(111)

        numOfBarGroups = len(METRICS)
        barWidth = (1.5 / numOfBarGroups) - (3 * BARSPACING)

        labels = []
        bars = []
        # group is which bars we are plotting, that would be a window
        hatches = ['.', '+', 'x', '\\', '-', 'o', 'O', '*']

        group = 1

        for label in scoresPerLabel.keys():
            labels.append(label)
            xLocations = self._calculateXLocations(group, barWidth)
            color = cm.terrain(0.0 + 0.05 * (group - 1))
            values = scoresPerLabel[label]
            hatch = hatches[group - 1]
            self._plotGroupBars(ax, xLocations, values, barWidth, color, hatch, bars)
            group += 1

        self._setXYLimits(ax, barWidth)

        self._setXTicks(ax, barWidth)
        title = "Metric Scores for The %s Classifier with a %s Window Size \n for The Top Labels By Count " % (classifierName, windowSize)
        self._setLabelsLegend(ax, bars, labels, title)

        plt.savefig("%s.png" % os.path.join(self.outDir, "metrics-topLabels-%s-%s" % (classifierName, windowSize)))
        plt.close()
        plt.clf()

    @staticmethod
    def _calculateXLocations(classifierCount, barWidth):
        return [a - (0.5 - BARSPACING) + (classifierCount * (barWidth + BARSPACING)) for a in XRANGE]

    @staticmethod
    def _setXYLimits(ax, barWidth):
        ax.autoscale(tight=True)
        ax.set_ylim([0.0, 1.0])
        plt.yticks(np.arange(0.0, 1.0, 0.1))
        x1, x2, _, _ = ax.axis()
        ax.set_xlim(x1 - barWidth, x2 + barWidth)

    @staticmethod
    def _setXTicks(ax, barWidth):
        ax.set_xticks(XRANGE + barWidth)
        ax.set_xticklabels(METRICS)

    @staticmethod
    def _setLabelsLegend(ax, bars, labels, title):
        ax.set_ylabel("Score")
        ax.set_xlabel("Metric")
        ax.legend([bar[0] for bar in bars], labels, loc=4, prop={"size": 9})
        ax.set_title(title)

    @staticmethod
    def _plotGroupBars(ax, xLocations, values, barWidth, color, hatch, bars):
        bars.append(ax.bar(xLocations, values, width=barWidth, align="center", color=color, hatch=hatch))

    @staticmethod
    def _setItemColor(box, item, color):
        for b in box[item]:
            plt.setp(b, color=color, linestyle="-")

    def setBoxColors(self, box, color):
        self._setItemColor(box, "boxes", "black")
        self._setItemColor(box, "caps", "black")
        self._setItemColor(box, "whiskers", "black")
        self._setItemColor(box, "fliers", "black")
        self._setItemColor(box, "medians", color)
