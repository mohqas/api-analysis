# Not used, deprecated?
import os

from analyzer.visualizers.MultiVisualizer import MultiVisualizer
import matplotlib.pyplot as plt


class RobustnessAcrossChangePlot(MultiVisualizer):
    def __init__(self, outputDir, dataset, dataSource):
        super(RobustnessAcrossChangePlot, self).__init__(outputDir)
        self.dataset = dataset
        self.ds = dataSource

    def createOutput(self, resultsDict):
        # FUTURE WORK: Validate the structure of the input
        colors = ['b', 'g', 'r', 'c', 'm', 'y']
        shapes = ['o', 'v', '^', 's', 'p', '*', 'h', 'H', 'd', 'D']

        changePercentages = sorted(resultsDict.values[0].keys())

        for windowSize in resultsDict:
            color = colors[resultsDict.keys().index(windowSize)]
            shape = shapes[resultsDict.keys().index(windowSize)]
            accuracies = [resultsDict[windowSize][c] for c in changePercentages]
            plt.plot(changePercentages, accuracies, color=color, marker=shape, label=windowSize)

        plt.title("Classification Accuracy Robustness - %s" % self.ds)
        plt.xlabel("Percentage of Change")
        plt.ylabel("Classification Accuracy")
        plt.ylim(0, 100)
        plt.legend(loc=4)
        plt.grid()

        if not os.path.exists(self.outDir):
            os.makedirs(self.outDir)

        plt.savefig("%s.png" % os.path.join(self.outDir, "accuracy-robustness-%s_%s" % self.dataset))
