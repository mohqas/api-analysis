import os
import numpy as np
from analyzer.visualizers.SingleVisualizer import SingleVisualizer
from matplotlib import rcParams

rcParams.update({'figure.autolayout': True})
from matplotlib import pyplot as plt


class AccuracyMatrixPlot(SingleVisualizer):
    def createOutput(self, accuracyInfo, accuracyMatrix):
        """
        :param accuracyInfo:
        :param accuracyMatrix: parameters: labels / matrix
        :return:
        """
        fig, ax = plt.subplots()
        hMap = ax.pcolor(accuracyMatrix.matrix, cmap=plt.cm.Blues)

        # put the major ticks at the middle of each cell
        ax.set_title('Classification Accuracy Map \n %s Classifier' % self.classifier)
        ax.spines['left'].set_position(('outward', 10))
        ax.spines['bottom'].set_position(('outward', 10))
        ax.set_xticks(np.arange(accuracyMatrix.matrix.shape[0]) + 0.5, minor=False)
        ax.set_yticks(np.arange(accuracyMatrix.matrix.shape[1]) + 0.5, minor=False)

        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.invert_yaxis()

        ax.set_xlabel("Training Data Labels")
        ax.set_ylabel("Classifier Labels")

        ax.set_xticklabels(accuracyMatrix.labels, minor=False)
        ax.set_yticklabels(accuracyMatrix.labels, minor=False)
        cBar = plt.colorbar(hMap)
        cBar.set_label("Accuracy (%)")
        plt.setp(ax.xaxis.get_majorticklabels(), rotation=90)

        if not os.path.exists(self.outDir):
            os.makedirs(self.outDir)

        fig.savefig("%s.png" % os.path.join(self.outDir, "accuracy-map_%s_%s_%s" % ("-".join(self.classifier.split(" ")), self.dataset,
                                                                                    self.windowSize)))
        plt.clf()
