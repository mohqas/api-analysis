class MultiVisualizer(object):
    def __init__(self, outputDir):
        self.outDir = outputDir

    def createOutput(self, resultsDict, *args, **kwargs):
        """
        :param resultsDict: A dictionary of the form {label: {x value: y value}, label ...etc}
        :return:
        """
        raise NotImplementedError
