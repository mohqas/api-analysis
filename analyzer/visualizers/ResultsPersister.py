import pickle
import os
from analyzer.visualizers.MultiVisualizer import MultiVisualizer


class ResultsPersister(MultiVisualizer):
    def __init__(self, outputDir, mode="normal-analysis"):
        super(ResultsPersister, self).__init__(outputDir)
        self.file = "%s.results" % os.path.join(self.outDir, mode)

    def createOutput(self, resultsDict, *args, **kwargs):
        pickle.dump(resultsDict, open(self.file, 'w'))

    def loadResults(self):
        return pickle.load(open(self.file))
