import unittest
import numpy as np
from analyzer.visualizers.EvaluationBarsPlotter import EvaluationBarsPlotter


class EvaluationBarsPlotter_Test(unittest.TestCase):
    def test_confusionCalculation_forAGivenConfusionMatrix(self):
        # [0, 1, 2] , [3, .... 7, 8]
        matrix = np.arange(9).reshape(3, 3)

        # expected
        TTP = 12
        TFP = 24
        TFN = 24
        TTN = 48

        self.assertEqual((TTP, TFP, TFN, TTN),
                         EvaluationBarsPlotter("", "")._calculateTotalConfusionFromConfusionMatrix(matrix))

    def test_scoresCalculation_forAGivenConfusionMatrix(self):
        matrix = np.arange(9).reshape(3, 3)

        expected = [0.3333333333333333, 0.3333333333333333, 0.3333333333333333, 0.]
        self.assertEqual(EvaluationBarsPlotter("", "")._calculateScoresForConfusionMatrix(matrix), expected)
