import sys
from analyzer.visualizers.SingleVisualizer import SingleVisualizer


class TextVisualizer(SingleVisualizer):
    def __init__(self, outputDirectory, classifier, dataset, windowSize, outputTarget=sys.stdout):
        super(TextVisualizer, self).__init__(outputDirectory, classifier, dataset, windowSize)
        self.ot = outputTarget

    def createOutput(self, accuracyInfo, accuracyMatrix):
        self.ot.write("Accuracy for classifier \"%s\" on dataset \"%s\", window size: %s, is: %f \n" % (
            self.classifier, self.dataset, self.windowSize, accuracyInfo.accuracy))
