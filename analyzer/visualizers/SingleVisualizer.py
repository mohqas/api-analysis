
class SingleVisualizer(object):
    def __init__(self, outputDirectory, classifier, dataset, windowSize):
        self.classifier = classifier
        self.dataset = dataset
        self.outDir = outputDirectory
        self.windowSize = windowSize

    # TODO: Switch to classifier results namedtuple? or will that make it classifier specific?
    def createOutput(self, accuracyInfo, accuracyMatrix):
        raise NotImplementedError
