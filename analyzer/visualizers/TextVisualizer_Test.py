import numpy as np
from unittest import TestCase
from analyzer.processes.Classifier import AccuracyMatrix, MissPrediction

# Testing values
MissPredictions = [MissPrediction(item=2, label=1, predictedLabel=2)]
InequalityMatrix = AccuracyMatrix(labels=[1, 2], matrix=np.array([[50.,   50.], [0.,  100.]]))


class TestTextVisualizer(TestCase):
    def test_createOutput_printsInformationAsExpectedAbove(self):
        pass
