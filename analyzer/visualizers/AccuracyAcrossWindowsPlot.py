import os

from analyzer.visualizers.MultiVisualizer import MultiVisualizer
import matplotlib.pyplot as plt


class AccuracyAcrossWindowsPlot(MultiVisualizer):
    def __init__(self, outputDir, dataset):
        super(AccuracyAcrossWindowsPlot, self).__init__(outputDir)
        self.dataset = dataset

    def createOutput(self, resultsDict, *arg, **kwargs):
        # FUTURE WORK: Validate the structure of the input
        colors = ['b', 'g', 'r', 'c', 'm', 'y']
        shapes = ['o', 'v', '^', 's', 'p', '*', 'h', 'H', 'd', 'D']

        windowSizes = sorted(resultsDict.values()[0].keys())

        for classifier in resultsDict:
            color = colors[resultsDict.keys().index(classifier)]
            shape = shapes[resultsDict.keys().index(classifier)]
            accuracies = [resultsDict[classifier][w].accuracyInfo.accuracy for w in windowSizes]
            plt.plot(windowSizes, accuracies, color=color, marker=shape, label=classifier)

        if "title" in kwargs:
            plt.title(kwargs["title"])
        else:
            plt.title("Classification Accuracy for Different Window Sizes")
        if "xlabel" in kwargs:
            plt.xlabel(kwargs["xlabel"])
        else:
            plt.xlabel("Window Size")
        if "ylabel" in kwargs:
            plt.ylabel(kwargs["ylabel"])
        else:
            plt.ylabel("Classification Accuracy")
        plt.ylim(0, 100)
        plt.legend(loc=4)
        plt.grid()

        if not os.path.exists(self.outDir):
            os.makedirs(self.outDir)

        plt.savefig("%s.png" % os.path.join(self.outDir, "accuracy-per-window_%s" % self.dataset))
        plt.clf()
