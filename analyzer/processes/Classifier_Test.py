from mock import Mock
from unittest import TestCase
from analyzer.processes.Classifier import Classifier, MissPrediction, AccuracyMatrix
from analyzer.retriever.MongoDbSequences import ProcessInfo


class ClassifierMock(object):
    def __init__(self):
        self.dataDict = {}

    def fit(self, dataDict):
        for label, values in dataDict.items():
            for v in values:
                self.dataDict[v] = label

    def predict(self, value):
        return self.dataDict.get(value, 0)


class TestClassifier(TestCase):
    # Invalid test -> needs to adapt to process structure -> maybe extract the learning and retrieving of label? -> mock :/.
    # def test_testMethod_correctlyShowsTheTestingResults(self):
    #     classifier = ClassifierMock()
    #     classifier.fit({1: [[1], ["one"]], 2: [[2], ["two"]], 3: [[3], ["three"]]})
    #     expectedInequalities = [MissPrediction(item=2, label=1, predictedLabel=2)]
    #     expectedAccMatrix = AccuracyMatrix(labels=[1, 2], matrix=[[50.,   50.], [0.,  100.]])
    #
    #     c = Classifier(classifier, name="testClassifier")
    #     accInfo, accMat = c.test({1: [ProcessInfo(sequence=1, name=""), ProcessInfo(sequence=2, name="")],
    #                               2: [ProcessInfo(sequence=2, name=""), ProcessInfo(sequence="two", name="")]})
    #
    #     self.assertItemsEqual(accInfo.missPredictions, expectedInequalities)
    #     self.assertEqual(accInfo.accuracy, 75.)
    #
    #     self.assertItemsEqual(accMat.labels, expectedAccMatrix.labels)
    #     self.assertItemsEqual(accMat.matrix[1], expectedAccMatrix.matrix[1])
    #     self.assertItemsEqual(accMat.matrix[0], expectedAccMatrix.matrix[0])

    def test_trainMethod_passesTheDataToTheClassifierFitMethod(self):
        classifier = Mock(ClassifierMock())

        c = Classifier(classifier, name="testClassifier")
        c.train({})

        classifier.fit.assert_called_once_with([], [])

