from sklearn.ensemble import RandomTreesEmbedding
from sklearn.ensemble import VotingClassifier
from sklearn.model_selection import ShuffleSplit

from analyzer.processes.Process import Process
from collections import namedtuple
from sklearn import svm
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score
import numpy as np

MissPrediction = namedtuple("MissPrediction", "item label predictedLabel")

AccuracyMatrix = namedtuple("AccuracyMatrix", "labels matrix")
AccuracyInfo = namedtuple("AccuracyInfo", "accuracy testElementsCount missPredictions")
GtAndPrLabels = namedtuple("GtAndPrLabels", "predictedLabels groundTruth")

ClassifierResults = namedtuple("ClassifierResults", "accuracyInfo, accuracyMatrix, gtAndPrLabels")


class Classifier(Process):
    def __init__(self, classifier, name):
        super(Classifier, self).__init__()
        self.processor = classifier
        self.name = name

    def train(self, sequencesDict):
        trainingLabels, trainingSequences = self._transferDictToListsOfSequencesAndLabels(sequencesDict)
        self.processor.fit(trainingSequences, trainingLabels)

    def crossValidate(self, sequencesDict):
        trainingLabels, trainingSequences = self._transferDictToListsOfSequencesAndLabels(sequencesDict)
        cv = ShuffleSplit(n_iter=7, test_size=0.07, random_state=1)
        scores = cross_val_score(self.processor, trainingSequences, trainingLabels, cv=cv)
        return ClassifierResults(accuracyInfo=AccuracyInfo(accuracy=(scores.max() * 100.), testElementsCount=len(trainingLabels),
                                                           missPredictions=None),
                                 accuracyMatrix=AccuracyMatrix(labels=None, matrix=None),
                                 gtAndPrLabels=GtAndPrLabels(predictedLabels=None, groundTruth=None))

    @staticmethod
    def _transferDictToListsOfSequencesAndLabels(sequencesDict):
        trainingSequences = []
        trainingLabels = []
        for label in sequencesDict:
            for sequence in sequencesDict[label]:
                trainingSequences.append(sequence.sequence)
                trainingLabels.append(label)
        return trainingLabels, trainingSequences

    def test(self, sequencesDict):
        """
        :param sequencesDict: The training correctly labeled data points
        :return: Two values, the inequality list, and the accuracy matrix, where the y side is the data labels,
        and the x side is the prediction labels. eg
           A   B
        A  50  50
        B  0   100
        """
        labels = sequencesDict.keys()
        testElementsCount = sum(map(len, sequencesDict.values()))
        matrixLen = len(labels)
        unequals = []
        accuracyMatrix = np.zeros(shape=(matrixLen, matrixLen), dtype=float)
        groundTruthLabels = []
        predictedLabels = []

        for label in labels:
            labelSeqCount = len(sequencesDict[label])
            # This can be refactored to test against the whole set of samples in a label
            for item in sequencesDict[label]:
                predictedLabel = str(self.processor.predict([item.sequence])[0])
                accuracyMatrix[labels.index(label)][labels.index(predictedLabel)] += 1. * 100 / labelSeqCount

                groundTruthLabels.append(label)
                predictedLabels.append(predictedLabel)

                if label != predictedLabel:
                    unequals.append(MissPrediction(item=item, label=label, predictedLabel=predictedLabel))

        return ClassifierResults(accuracyInfo=AccuracyInfo(accuracy=self._calculateAccuracy(testElementsCount, unequals),
                                                           testElementsCount=testElementsCount, missPredictions=unequals),
                                 accuracyMatrix=AccuracyMatrix(labels=labels, matrix=accuracyMatrix),
                                 gtAndPrLabels=GtAndPrLabels(predictedLabels=predictedLabels, groundTruth=groundTruthLabels))

    @staticmethod
    def _calculateAccuracy(testElementsCount, unequals):
        return 100.0 - (len(unequals) * 100. / testElementsCount)


# TODO create a voting classifier
class ClfClassifier(Classifier):
    def __init__(self):
        clf = MLPClassifier(
            activation='relu', algorithm='l-bfgs', alpha=1e-05,
            batch_size=5, beta_1=0.9, beta_2=0.999, early_stopping=False,
            epsilon=1e-08, hidden_layer_sizes=(500, 20), learning_rate='constant',
            learning_rate_init=0.001, max_iter=200, momentum=0.9,
            nesterovs_momentum=True, power_t=0.5, random_state=1, shuffle=True,
            tol=0.0001, validation_fraction=0.1, verbose=False,
            warm_start=False)
        super(ClfClassifier, self).__init__(clf, "MLP")


class LinearSVMClassifier(Classifier):
    def __init__(self):
        clSvmLiear = svm.LinearSVC()
        super(LinearSVMClassifier, self).__init__(clSvmLiear, "Linear SVM")


class NonLinearSVMClassifier(Classifier):
    def __init__(self):
        clSvm = svm.SVC(kernel="rbf", C=4.0)
        super(NonLinearSVMClassifier, self).__init__(clSvm, "Nonlinear SVM")


class RFClassifier(Classifier):
    def __init__(self):
        rfc = RandomForestClassifier(n_estimators=25, random_state=1, criterion="entropy")
        super(RFClassifier, self).__init__(rfc, "Random Forest")


# Not used!
class RTEClassifier(Classifier):
    def __init__(self):
        rfe = RandomTreesEmbedding(n_estimators=25, random_state=1)
        super(RTEClassifier, self).__init__(rfe, "Random Trees")


class EnsembleVoting(Classifier):
    def __init__(self):
        # clf = MLPClassifier(
        #     activation='relu', algorithm='l-bfgs', alpha=1e-05,
        #     batch_size=5, beta_1=0.9, beta_2=0.999, early_stopping=False,
        #     epsilon=1e-08, hidden_layer_sizes=(400, 10), learning_rate='constant',
        #     learning_rate_init=0.001, max_iter=200, momentum=0.9,
        #     nesterovs_momentum=True, power_t=0.5, random_state=1, shuffle=True,
        #     tol=0.0001, validation_fraction=0.1, verbose=False,
        #     warm_start=False)
        clSvm = svm.SVC(kernel="rbf", C=4.0)
        rfc = RandomForestClassifier(n_estimators=25, random_state=1, criterion="entropy")
        clSvmLiear = svm.LinearSVC()

        eclf = VotingClassifier(estimators=[('svml', clSvmLiear), ('rfc', rfc), ('svm', clSvm)], voting='hard')

        super(EnsembleVoting, self).__init__(eclf, "Voting Classifier")
