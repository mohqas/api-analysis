import pickle


class Process(object):
    def __init__(self):
        self.processor = None

    def save(self, filePath):
        pickle.dump(self.processor, open(filePath, 'w'))

    def load(self, filePath):
        self.processor = pickle.load(open(filePath))

    def train(self, sequencesDict):
        raise NotImplementedError

    def test(self, sequencesDict):
        raise NotImplementedError
