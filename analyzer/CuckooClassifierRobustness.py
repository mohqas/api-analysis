import os

from acquisition.persisters.mongodb.CallsPersister import CallsPersister
from analyzer.retriever.MongoDbSequences import MongoDbSequences
from analyzer.processes.Classifier import ClfClassifier, LinearSVMClassifier, NonLinearSVMClassifier, RFClassifier, EnsembleVoting
from analyzer.representer.EcsTransformer import EcsTransformer
from analyzer.representer.RobustnessNoOpsInsertion import RobustnessNoOpsInsertion
from analyzer.representer.RobustnessRandomReplace import RobustnessRandomReplace
from analyzer.representer.RobustnessReorder import RobustnessReorder
from analyzer.visualizers.AccuracyAcrossWindowsPlot import AccuracyAcrossWindowsPlot
from analyzer.visualizers.AccuracyMatrixPlot import AccuracyMatrixPlot
from analyzer.visualizers.EvaluationBarsPlotter import EvaluationBarsPlotter
from analyzer.visualizers.ResultsPersister import ResultsPersister
from analyzer.visualizers.TextVisualizer import TextVisualizer
from configurations.database import DatabaseConfiguration
from configurations.results import ResultsConfiguration

ApiCountToConsider = 500
#Classifiers = [EnsembleVoting, LinearSVMClassifier, NonLinearSVMClassifier, RFClassifier]
Classifiers = [RFClassifier]
SingleVisualizers = [TextVisualizer, AccuracyMatrixPlot]
MultiVisualizers = [ResultsPersister, AccuracyAcrossWindowsPlot, EvaluationBarsPlotter]
TestWindows = [10, 150, 300]
ChangePercentages = [0, 10, 20, 40]
ApiIndexSize = CallsPersister(DatabaseConfiguration()).getTotalNumberOfPersistedCalls()
RobustnessTests = [lambda x: RobustnessReorder(ApiCountToConsider, 3, x),
                   lambda x: RobustnessNoOpsInsertion(ApiCountToConsider, ApiIndexSize, x),
                   lambda x: RobustnessRandomReplace(ApiCountToConsider, ApiIndexSize, x)]


class CuckooClassifierRobustness(object):
    def __init__(self, datasetName, apisToConsider, window, classifyPerVersion, outputDir):
        self.versions = classifyPerVersion
        self.window = window
        self.datasetName = datasetName
        self.apisToConsider = apisToConsider
        self.sequencesRetriever = MongoDbSequences(minimumApiCount=self.apisToConsider, perVersion=self.versions)
        self.outDir = outputDir

    def _acquireTraining(self):
        return self.sequencesRetriever.acquireTrainingSequencesDictionary()

    def _acquireTesting(self):
        return self.sequencesRetriever.acquireTestSequencesDictionary()

    def _represent(self, sequencesDict):
        totalNumberOfApis = CallsPersister(DatabaseConfiguration()).getTotalNumberOfPersistedCalls()
        return EcsTransformer(totalNumberOfApis, self.window).transform(sequencesDict)

    @staticmethod
    def _train(classifiers, trainingSequences):
        for classifier in classifiers:
            classifier.train(trainingSequences)

    @staticmethod
    def _test(classifiers, testingSequences):
        # type: (Classifier, Dict) -> Dict
        results = {}
        for classifier in classifiers:
            results[classifier.name] = classifier.test(testingSequences)

        return results

    def trainClassifiers(self, classifiers):
        trainingEcses = self._represent(self._acquireTraining())
        self._train(classifiers, trainingEcses)

    def testClassifiersRobustness(self, classifiers, robustnessTests):
        testingSequences = self._acquireTesting()
        totalResults = {}

        for rt in robustnessTests:
            testingEcses = self._represent(rt.transform(testingSequences))
            results = self._test(classifiers, testingEcses)
            totalResults[rt.name] = results

        return totalResults

    def persist(self, classifiers):
        for c in classifiers:
            c.save(os.path.join(self.outDir, "%s_%s" % (c.name, self.window)))


def main(version):
    datasetName = DatabaseConfiguration().analysisDatabase
    resultsPerRobustness = {}
    outputsDir = os.path.join(ResultsConfiguration().resultsLocation, datasetName, "robustness")
    if not os.path.exists(outputsDir):
        os.makedirs(outputsDir)

    classifiers = [c() for c in Classifiers]

    for window in TestWindows:
        facade = CuckooClassifierRobustness(datasetName, ApiCountToConsider, window, version, outputsDir)
        facade.trainClassifiers(classifiers)
        facade.persist(classifiers)

        for percentage in ChangePercentages:
            results = facade.testClassifiersRobustness(classifiers, [r(percentage) for r in RobustnessTests])
        # An ugly update process, but we'll let it be for now for time constrains.
            for robustness in results:
                if robustness in resultsPerRobustness:
                    if window in resultsPerRobustness[robustness]:
                        resultsPerRobustness[robustness][window].update({percentage: results[robustness][classifiers[0].name]})
                    else:
                        resultsPerRobustness[robustness][window] = {percentage: results[robustness][classifiers[0].name]}
                else:
                    resultsPerRobustness[robustness] = {window: {percentage: results[robustness][classifiers[0].name]}}

    for visualizer in MultiVisualizers:
        for test in resultsPerRobustness:
            v = visualizer(outputsDir, "%s-%s" % (datasetName, test))
            v.createOutput(resultsDict=resultsPerRobustness[test], title="Classification Accuracy Robustness Against: %s" % test,
                           xlabel="Change Percentage in Input", ylabel="Classification Accuracy")

if __name__ == "__main__":
    main(version=DatabaseConfiguration().isVersionDatabase)
